<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'vendstore' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Xy+aA*1MP,TNM/JG*6g}VQ!5;Tr4@kJTz!3X^xD]j?kib`,&c5==7h04I lAoM74' );
define( 'SECURE_AUTH_KEY',  'Yv#ujx$`5BY.Di5L %E=XOd{4:-&A.g5@`v](~L*rupP<(2X7U~v$- VXUwtd!F[' );
define( 'LOGGED_IN_KEY',    '7!0(Z+sQ$`,X[DV-U!e!QN8@7(6D:?%rPe,_)INLMVVuo(FHfAf;JNv@if/KO#I`' );
define( 'NONCE_KEY',        'My33[(Q>M%8u8m!LoX.w)2eD4y/6n@~t/vkZR(sU/i,;lz2#4KS+WN8*A.Vd*7;5' );
define( 'AUTH_SALT',        'xyF`I4scP#x. 4^yr35?@tT&*gz;A72<[qaT!sN+p)1oO_0K=:F|Tp4UWdK]h=$8' );
define( 'SECURE_AUTH_SALT', '~g]l^}uodh;054}Rku0{gig (2ouSw0:6uN=JX/S?#$Mt]QgV[Pi`JQxtEMl0nsc' );
define( 'LOGGED_IN_SALT',   '{J@)BYL%SdIX]%xg*n8Y<Ckq?W-4+36vA~*1PQy;.Z^Kdz0Y+hay;tm3COkL=]bf' );
define( 'NONCE_SALT',       '2z{*.8yzWEcU_z~$ar^/~%z?uxRM7R.ee$_yIB^1?#AUcFCtCx!*F&r`WXqx;O a' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
